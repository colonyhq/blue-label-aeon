import socket, ssl
import uuid

from datetime import datetime

from .response import AeonResponse
from .constants import (
    TOPUP_AUTH_XML, TOPUP_REPRINT_XML, TOPUP_XML
)


class AeonAirtime():
    """
    A simple class to the interface with the Blue Label Aeon service. This allows you to top up a user's airtime for
    the cellphone.
    """
    BUFFER_SIZE = 4096

    TRAN_TYPE_CELL_C = 'CellC'
    TRAN_TYPE_MTN = 'MTN'
    TRAN_TYPE_8TA = 'TelkomMobile'
    TRAN_TYPE_VODACOM = 'Vodacom'

    def __init__(self, tran_type, host, port, user_pin, device_id, device_serial):
        """
        """
        self.tran_type = tran_type
        self.session_id = ''
        self.ref = uuid.uuid1()
        self.response = AeonResponse()

        self.host = host
        self.port = port

        # Messages.
        self.auth_message = TOPUP_AUTH_XML % {
            'user_pin': user_pin,
            'device_id': device_id,
            'device_serial': device_serial,
            'tran_type': self.tran_type,
            'ref': self.ref
        }

    def __drain_sock(self, sock):
        """
        """
        data = ''
        chunk = sock.recv(self.BUFFER_SIZE)

        while len(chunk):
            data += chunk

            if '\n' not in data:
                chunk = sock.recv(self.BUFFER_SIZE)
            else:
                chunk = ''

        return data

    def __do_call(self, message):
        """
        """
        # Do calls through socket.
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
        context.options |= ssl.OP_NO_TLSv1 | ssl.OP_NO_TLSv1_1

        try:
            sock = context.wrap_socket(sock, server_hostname=self.host)
            sock.connect((self.host, self.port))

            # First we need to do an Auth Request.
            sock.send(self.auth_message)
            self.response.add_response(xml_string=self.__drain_sock(sock))

            if not self.response.has_error():
                self.session_id = self.response.session_id
                sock.send(message)
                self.response.add_response(xml_string=self.__drain_sock(sock))

            return self.response

        finally:
            sock.close()

    def do_topup(self, phone_no, amount, product_name, authoriser):
        """
        This will request a specific Topup from the system. The network will depend on the TransType you used to
        authenticate with (Vodacom, CellC, MTN, TelkomMobile, TelkomFixedLine, TelkomWorldCall).
        """
        message = TOPUP_XML % {
            'session_id': self.session_id,
            'ref': self.ref,
            'phone_no': phone_no,
            'amount': amount,
            'product_name': product_name,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'date': datetime.now().strftime('%Y-%m-%d'),
            'authoriser': authoriser
        }

        return self.__do_call(message)

    def do_reprint(self, phone_no, reference):
        """
        Since voucher and top up transactions cannot be reversed, reprints are available so that these transactions can
        be received and printed in case they got lost.
        """
        message = TOPUP_REPRINT_XML % {
            'session_id': self.session_id,
            'reference': reference,
            'phone_no': phone_no
        }

        return self.__do_call(message)