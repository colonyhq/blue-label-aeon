import xmltodict

from xml.etree import ElementTree
from aeon.xml_utils import XmlDictConfig


class AeonResponse():
    """
    """
    def __init__(self):
        """
        """
        self.data = {}
        self.session_id = ''
        self.ref = ''
        self.is_error = True
        self.error_code = 0
        self.error_text = ''

    def __parse_xml(self, xml_string):
        """
        """
        if not len(xml_string):
            self.error_code = 99999
            self.error_text = 'No XML returned'
            return

        doc = ElementTree.fromstring(xml_string)

        # Set some vars.
        self.session_id = doc.find('SessionId').text
        self.is_error = doc.find('event/EventCode').text != '0'

        # Set error data if there is an error.
        if self.is_error:
            self.error_code = doc.find('data/ErrorCode').text
            self.error_text = doc.find('data/ErrorText').text

        else:
            # Get transaction Ref.
            ref = doc.find('data/Reference')
            if ref:
                self.ref = ref.text
            else:
                self.ref = ''

            # Get data returned.
            data_xml = ElementTree.tostring(doc.find('data'), encoding='utf8', method='xml')
            data = xmltodict.parse(data_xml)
            self.data.update(data)

    def add_response(self, xml_string):
        """
        """
        self.__parse_xml(xml_string=xml_string)

    def has_error(self):
        """
        """
        return self.is_error