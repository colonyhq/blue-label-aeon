from aeon.airtime import AeonAirtime
from aeon.response import AeonResponse


AEON_HOST = '196.26.170.3'
AEON_PORT = 7897
AEON_USER_PIN = '011234'
AEON_DEVICE_ID = '1750'
AEON_DEVICE_SERIAL = 'ECOM'

PHONE_NO = '27813821298'
TOPUP_AMOUNT = 5
PRODUCT_NAME = 'Blue Label API Test'


_globals = {'airtime_resp': None}


def test_airtime_topup():
    """
    """
    aeon = AeonAirtime(
        tran_type=AeonAirtime.TRAN_TYPE_8TA,
        host=AEON_HOST,
        port=AEON_PORT,
        user_pin=AEON_USER_PIN,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL
    )
    airtime_resp = aeon.do_topup(
        phone_no=PHONE_NO,
        amount=TOPUP_AMOUNT,
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    _globals['airtime_resp'] = airtime_resp

    assert isinstance(airtime_resp, AeonResponse)
    assert airtime_resp.has_error() == False