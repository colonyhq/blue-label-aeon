from aeon.data import AeonData
from aeon.response import AeonResponse


AEON_HOST = '196.38.158.118'
AEON_PORT = 7898
AEON_USER_PIN = '011234'
AEON_DEVICE_ID = '1750'
AEON_DEVICE_SERIAL = 'ECOM'


PHONE_NO = '27813821298'
TOPUP_AMOUNT = 5
PRODUCT_NAME = 'Blue Label Data API Test'


_globals = {'data_resp': None}


def test_data():
    """
    """
    aeon = AeonData(
        tran_type=AeonData.TRAN_TYPE_8TA,
        host=AEON_HOST,
        port=AEON_PORT,
        user_pin=AEON_USER_PIN,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL
    )
    data_resp = aeon.get_products(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    _globals['data_resp'] = data_resp

    assert isinstance(data_resp, AeonResponse)
    assert data_resp.has_error() == False

    data_resp = aeon.do_topup(
        phone_no='0813821298',
        product_code='755',
        product_name=PRODUCT_NAME,
        reference='',
        authoriser='Testing'
    )

    _globals['data_resp'] = data_resp

    assert isinstance(data_resp, AeonResponse)
    assert data_resp.has_error() == False

test_data()