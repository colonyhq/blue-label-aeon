from nose.tools import *

from aeon.electricity import AeonElectricity
from aeon.response import AeonResponse


AEON_HOST = '196.26.170.3'
AEON_PORT = 7893
AEON_USER_PIN = '011234'
AEON_DEVICE_ID = '1750'
AEON_DEVICE_SERIAL = 'ECOM'

METER_NO = '01060029601'
VOUCHER_AMOUNT = 15
PRODUCT_NAME = 'Blue Label API Test'


@raises(KeyError)
def test_bsst_elec_voucher():
    """
    01050020006
        Actaris,Returns a single BSST (FBE) Token and no valued tokens or arrears/costs.
    """
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01050020006',
        amount=VOUCHER_AMOUNT,
        ignore_debts=True,
        ignore_free_tokens=True
    )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['BSSTTokens']['BSSTToken']['Number']) > 0

    electricity_resp.data['Tokens']

@raises(KeyError)
def test_debt_collection_elec_voucher():
    """
    01060029601
        Actaris,"Whatever the vend amount, the entire amount will be consumed by debt collection, and no tokens will be
        vended."
    """
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029601',
        amount=VOUCHER_AMOUNT,
    )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert electricity_resp.data['Debts']['Debt']['Reason'] == 'Debt Collection'
    assert electricity_resp.data['Debts']['Debt']['Remain'] == '50.00'

    electricity_resp.data['Tokens']

@raises(KeyError)
def test_min_vend_amt_elec_voucher():
    """
    01060029602
        Actaris,"Requires a minimum vend amount of R10. R10 will be consumed in arrears collection, and the remainder
        will be used to purchase a token. Lower vend amounts will return an error ""Amount too low""."
    """
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029602',
        amount=9,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == True
    assert electricity_resp.error_code == '401'

    electricity_resp.data['Tokens']

    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029602',
        amount=VOUCHER_AMOUNT,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0
    assert electricity_resp.data['Debts']['Debt']['Amount'] == '10.00'

def test_single_std_elec_voucher():
    """
    01060029603
        Actaris,"Returns a single standard token for half the vended amount, a fixed cost for a quarter, and a debt
        collection for the final quarter."

    01060029604
        Actaris,"A single standard token for three quarters the vended amount, and a single fixed charge for the
        remaining quarter."

    01060029605
        Actaris,"A single standard token for half the vended amount, and a single debt collection for the other half."

    01060029606
        Actaris,"A single token vended for the full amount of the transaction, and a single free token."

    01060029607
        Actaris,A single token.

    01060029608
        Actaris,"Vends a full transaction with multiple debts and fixed costs, a single standard token, and a free
        token. The value of the debts,  costs and token are each equal to one fifth of the vended amount."

    01060029609
        Actaris,"A single token with description "Normal Sale"
    """
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029603',
        amount=VOUCHER_AMOUNT,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0
    assert electricity_resp.data['Debts']['Debt']['Amount'] == '3.75'

    # ---
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029604',
        amount=VOUCHER_AMOUNT,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0
    assert electricity_resp.data['FixedCosts']['Fixed']['Amount'] == '3.23'

    # ---
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029605',
        amount=VOUCHER_AMOUNT,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0
    assert electricity_resp.data['Debts']['Debt']['Amount'] == '7.50'

    # ---
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029606',
        amount=VOUCHER_AMOUNT,
    )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0
    assert len(electricity_resp.data['BSSTTokens']['BSSTToken']['Number']) > 0

    # ---
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029607',
        amount=VOUCHER_AMOUNT,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0

    # ---
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029608',
        amount=VOUCHER_AMOUNT,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0

    # ---
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029609',
        amount=VOUCHER_AMOUNT,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0

def test_debt_cancelation_elec_voucher():
    """
    01060029610
        Actaris,"Has an outstanding debt value of R50. Each purchase will reduce the remaining debt. If no debt remains,
        a token for the requested amount will be returned. The remaining debt will reset after 10 minutes to allow
        repeated tests."
    """
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029610',
        amount=25,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False

    # ---
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029610',
        amount=25,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False

    # ---
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029610',
        amount=25,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0

def test_tshwane_elec_voucher():
    """
    01060029500
        Tshwane,Tshwane test meter. Responds with information and formatting the same as a Tshwane meter would.
    """
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029500',
        amount=VOUCHER_AMOUNT,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0

def test_tshwane_reduce_debt_elec_voucher():
    """
    01060029501
        Tshwane,Tshwane test meter with arrears. Works the same as the reducing debt test.
    """
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029501',
        amount=VOUCHER_AMOUNT,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0

def test_eskom_elec_voucher():
    """
    01060029502
        Eskom,"Eskom test. Returns minimal customer information response, and includes Eskom tariff strings."
    """
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no='01060029502',
        amount=VOUCHER_AMOUNT,
        )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert len(electricity_resp.data['Tokens']['Token']['Number']) > 0

@raises(KeyError)
def test_ims_elec_voucher():
    """
    01700000001
        Ekurhuleni,IMS-style test. This meter does not return a token number - the token in this situation is sent
        directly to the customer's meter.
    """
    aeon = AeonElectricity(
        host=AEON_HOST,
        port=AEON_PORT,
        device_id=AEON_DEVICE_ID,
        device_serial=AEON_DEVICE_SERIAL,
        user_pin=AEON_USER_PIN,
        meter_no=METER_NO,
        amount=VOUCHER_AMOUNT,
    )
    electricity_resp = aeon.get_voucher(
        product_name=PRODUCT_NAME,
        authoriser='Testing'
    )

    assert isinstance(electricity_resp, AeonResponse)
    assert electricity_resp.has_error() == False
    assert electricity_resp.data['Debts']['Debt']['Amount'] == '15.00'

    electricity_resp.data['Tokens']